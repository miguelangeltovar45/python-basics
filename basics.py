import datetime
print("Variable assignation")
msj = "Date and time is: "
currentDateTime = datetime.datetime.now()
print(msj,currentDateTime)
print("Simple types")
x = 10
y = '10'
z = 10.1

sum1 = x + x
sum2 = y + y

print(sum1, sum2)
print(type(x), type(y) , type(z))

print('List Types')

monday_temperatures = [9.1, 8.8, 7.5]
print('student grades::: ', monday_temperatures)
listOfNum = list(range(1, 10))
print('List function::: ', listOfNum)

print('Useful commands')
print('dir(function) :: shows all properties and functions inside a function')
print('Ex:::', dir(list))
print('help() :: shows the documentation for a function')
print('Ex::: help(list)')
# print('Ex:::', help(list))

print('Dictionary::')
student_grades = {"Marry": 9.1, "Sim": 8.8, "jhon": 7.5}
mySum = sum(student_grades.values())
length = len(student_grades)
mean = mySum / length
print(mean)

print('tuple type::')
monday_temperatures = {9.1, 8.8, 7.5}
print('the difference between the list is the unmutability of the tuple, it means the tuple cannot be modified in the '
      'execution')
print('tuple example::: ', monday_temperatures)
